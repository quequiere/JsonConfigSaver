﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Threading;

namespace JsonConfigSaver
{
    public abstract class JsonEcoConfig
    {
        private String name;
        private String pluginName;

        public JsonEcoConfig(String pluginName, String name)
        {
            this.name = name;
            this.pluginName = pluginName;
        }

        private String toJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        public void save()
        {

            Thread lsr = new Thread(() => saveSafe());
            lsr.Start();
        }

        private void saveSafe()
        {
            Console.WriteLine($"Starting saving data for: {pluginName}"); 

            DateTime startTime = DateTime.Now;
            String savingData = "./Configs/" + pluginName + "/" + this.name + ".json";
            System.IO.Directory.CreateDirectory("./Configs/" + pluginName + "/");
            File.WriteAllText(savingData, toJson());
            DateTime endTime = DateTime.Now;

            double diff = (endTime - startTime).TotalMilliseconds;

            Console.WriteLine($"Data was sucessfully saved: {savingData} in {diff} ms");
        }
        

        public Boolean exist()
        {
            if(File.Exists("./Configs/" + pluginName + "/" + this.name + ".json"))
            {
                return true;
            }
            return false;
        }

        private void updateNames(String name, String plugin)
        {
            this.name = name;
            this.pluginName = plugin;
        }

        public T reload<T>() where T : JsonEcoConfig
        {
            string text = System.IO.File.ReadAllText("./Configs/" + pluginName + "/" + this.name + ".json");
            T obj = JsonConvert.DeserializeObject<T>(text);
            this.postLoaded(obj);
            return obj;
        }

        public virtual void postLoaded<T>(T newOb) where T:JsonEcoConfig
        {
            newOb.updateNames(this.name, this.pluginName);
        }
    }
}
